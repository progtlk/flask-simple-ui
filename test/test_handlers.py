from flask_simple_ui.login import NullHandler


def test_null():
    h = NullHandler()
    assert h.logged_in, "Null should always be logged in"
