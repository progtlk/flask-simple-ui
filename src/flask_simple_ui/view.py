from typing import *

from jinja2 import Environment
from markupsafe import Markup


class Renderable:
    def __init__(self, field_id: str):
        self._id = field_id
    
    @property
    def id(self):
        return self._id
    
    def markup(self, env: Environment, info: Dict[str, Any]) -> Markup:
        raise NotImplementedError


class View:
    def __init__(self, widgets: List[Renderable]):
        self._widgets = widgets
    
    def add(self, widget: Renderable):
        self._widgets.append(widget)
        
    def clone(self):
        return type(self)(self._widgets.copy())
        
    def markup(self, env: Environment, info) -> List[Markup]:
        return [widget.markup(env, info) for widget in self._widgets]
