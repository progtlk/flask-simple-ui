from typing import *

from flask import request
from jinja2 import Environment
from markupsafe import Markup
from werkzeug.datastructures import CombinedMultiDict, ImmutableMultiDict
from wtforms import FormField
from wtforms.form import BaseForm

from .view import Renderable, View


class UIFormField(Renderable):
    @property
    def wtf(self) -> Optional[FormField]:
        raise NotImplementedError
    
    def markup(self, env: Environment, info: Dict[str, Any]) -> Markup:
        raise NotImplementedError


class Form(View):
    def add_csrf(self, secret, time=3600):
        # TODO: Do CSRF
        pass
    
    def form(self) -> BaseForm:
        fields = {}
        for field in self._widgets:
            if hasattr(field, "wtf"):
                ff = field.wtf
                if ff is not None:
                    fields[field.id] = ff
        form = BaseForm(fields)
        if request.files:
            form.process(request.files)
        if request.form:
            form.process(request.form)
        json = request.get_json()
        if json is not None:
            form.process(ImmutableMultiDict(json))
        
        return form
