from sys import version_info
from typing import *

from flask import Flask, request
from jinja2 import BaseLoader, PackageLoader, Environment

from .view import View
from .form import Form
from .login import NullHandler


class Info:
    def __init__(self, root):
        self._root = root
    
    @property
    def root(self):
        return self._root


class UI:
    def __init__(self, app: Flask, root: str = "", login_handler=NullHandler(),
                 loader: BaseLoader = PackageLoader("flask_simple_ui"), csrf_secret=None, csrf_time=3600):
        self._csrf_time = csrf_time
        self._csrf_secret = csrf_secret
        self._app = app
        self._root = root
        # TODO: Fix async
        # if version_info[1] >= 6:
        #     asynchronous = True
        # else:
        asynchronous = False
        self._jinja = Environment(autoescape=True, loader=loader, enable_async=asynchronous)
        self._get_login_status = None
        self._login_handler = login_handler
        
        self._register_login()
    
    @property
    def info(self):
        return Info(root=self._root)
    
    def form(self, admin_form: Form, template_args: Dict[str, Any] = None):
        if template_args is None:
            template_args = {}
        template = self._jinja.get_template("form.html")
        form_markup = admin_form.markup(self._jinja, self.info)
        return template.render(form=form_markup, **template_args)
    
    @property
    def login_handler(self):
        return self._login_handler
    
    @login_handler.setter
    def login_handler(self, value):
        self._login_handler = value
    
    def _register_login(self):
        def login_handler():
            if request.method == "POST":
                wtf_form = self._login_handler.login_form.form(secret=self._csrf_secret, limit=self._csrf_time)
                if wtf_form.validate():
                    return self._login_handler.login(self.info, wtf_form.data)
                else:
                    return "Bad parameters"
            
            return self.form(self._login_handler.login_form,
                             {"title": "Login - %s" % self._app.name, "heading": "Login"})
        
        self._app.add_url_rule("%s/login" % self._root, "%s/login" % self._root, login_handler, methods=["GET", "POST"])
        
        def logout_handler():
            return self._login_handler.logout()
        
        self._app.add_url_rule("%s/logout" % self._root, "%s/logout" % self._root, logout_handler, methods=["GET"])
    
    def input(self, route: str, ui_form: Form, require_login=True, template_args=None):
        if template_args is None:
            template_args = {"title": "%s" % self._app.name}
        elif "title" not in template_args:
            template_args["title"] = "%s" % self._app.name
        
        def annotator(callback: Callable[[Info, Dict[str, Any]], Any]):
            def inner():
                if require_login:
                    if not self._login_handler.logged_in:
                        return self._jinja.get_template("not_logged_in.html").render(root=self._root)
                form = ui_form.clone()
                form.add_csrf(self._csrf_secret, time=self._csrf_time)
                if request.method == "POST":
                    wtf_form = form.form()
                    wtf_form.validate()
                    return callback(self.info, wtf_form.data)
                
                return self.form(form, template_args)
            
            full_route = "%s%s" % (self._root, route)
            self._app.add_url_rule(full_route, full_route, inner, methods=["GET", "POST"])
            
            return callback
        
        return annotator
    
    def route(self, route, **kwargs):
        def annotator(callback):
            def inner(*args, **kwargs2):
                return callback(self.info, *args, **kwargs2)
            return self._app.add_url_rule("%s%s" % (self._root, route), callback.__name__, inner, **kwargs)
            
        return annotator
    
    def view(self, view: View, template_args: Dict[str, Any] = None):
        if template_args is None:
            template_args = {}
        template = self._jinja.get_template("view.html")
        view_markup = view.markup(self._jinja, self.info)
        return template.render(view=view_markup, **template_args)
