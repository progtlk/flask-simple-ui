from _sha512 import sha512
from typing import Union

from flask import Response, request
from werkzeug.utils import cached_property, redirect

from flask_simple_ui.output import ButtonGroup
from .form import Form
from .input import Password, Submit, Optional


class Handler:
    @property
    def login_form(self):
        raise NotImplementedError
    
    def login(self, info, data):
        raise NotImplementedError
    
    def logout(self):
        raise NotImplementedError
    
    @property
    def logged_in(self):
        raise NotImplementedError


class NullHandler(Handler):
    @cached_property
    def login_form(self):
        return Form([
            ButtonGroup("buttons", [Submit("submit", "Login")])
        ])
    
    def login(self, info, data):
        if "next" in data:
            return redirect(data["next"])
        return "No login required"
    
    def logout(self):
        if "next" in request.args:
            return redirect(request.args["next"])
        return "No logout required"
    
    @property
    def logged_in(self):
        return True


class PasswordHandler(Handler):
    def __init__(self, password: str, iv: Optional[Union[bytes, str]] = None):
        if isinstance(iv, str):
            self._iv = iv.encode()
        else:
            self._iv = iv
        self._password = self.hash(password)
    
    def hash(self, string: str):
        h = sha512(string.encode())
        if self._iv is not None:
            h.update(self._iv)
        return h.digest()
    
    @cached_property
    def login_form(self):
        return Form([
            Password("password", "Password", required=True),
            ButtonGroup("buttons", [Submit("submit", "Login")])
        ])
    
    def login(self, info, data):
        if self._password is None:
            return "Okay"
        elif self._password == self.hash(data["password"]):
            resp = Response("Okay")
            resp.set_cookie("password", data["password"], httponly=True)
            return resp
        return "Bad PW"
    
    def logout(self):
        resp = Response("Okay")
        resp.delete_cookie("password")
        return resp
    
    @property
    def logged_in(self):
        if self._password is None:
            return True
        elif self._password == request.cookies.get("password", None):
            return True
        return False
