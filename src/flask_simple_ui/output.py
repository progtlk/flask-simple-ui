from typing import *

from jinja2 import Environment
from markupsafe import Markup

from .view import Renderable


def style2str(style: Dict[str, str]) -> str:
    string = ""
    for key in style.keys():
        string += f"{key}: {style[key]};"
    return string


class Button(Renderable):
    def __init__(self, field_id: str, label: str):
        super().__init__(field_id)
        self._label = label
    
    def markup(self, env: Environment, info: Dict[str, Any]) -> Markup:
        raise NotImplementedError


class ButtonGroup(Renderable):
    def __init__(self, field_id: str, children: List[Button]):
        super().__init__(field_id)
        self._children = children
    
    def markup(self, env: Environment, info: Dict[str, Any]) -> Markup:
        rendered = []
        for child in self._children:
            rendered.append(child.markup(env, info))
        return Markup(env.get_template("fields/view/button_group.html").render(id=self.id, children=rendered))


class Table(Renderable):
    def __init__(self, field_id: str, table: List[List[Any]], numbered=True, head=None):
        super().__init__(field_id)
        self._table = table
        self._numbered = numbered
        self._head = head
    
    def markup(self, env: Environment, info: Dict[str, Any]) -> Markup:
        return Markup(
                env.get_template("fields/view/table.html").render(id=self.id, table=self._table,
                                                                  numbered=self._numbered,
                                                                  head=self._head))


class Paragraph(Renderable):
    def __init__(self, field_id: str, text: str, style: Optional[Dict[str, str]] = None):
        super().__init__(field_id)
        if style is None:
            style = {}
        self._text = text
        self._style = style
    
    def markup(self, env: Environment, info: Dict[str, Any]) -> Markup:
        return Markup(env.get_template("fields/view/paragraph.html").render(
                id=self.id,
                text=self._text,
                style=style2str(self._style)
        ))


class Heading(Renderable):
    def __init__(self, field_id: str, order: int, text: str, style: Optional[Dict[str, str]] = None):
        super().__init__(field_id)
        if style is None:
            style = {}
        self._order = order
        self._text = text
        self._style = style
    
    def markup(self, env: Environment, info: Dict[str, Any]) -> Markup:
        return Markup(env.get_template("fields/view/heading.html").render(
                id=self.id,
                order=self._order,
                text=self._text,
                style=style2str(self._style)
        ))


class Container(Renderable):
    def __init__(self, field_id: str, children: List[Renderable], style: Optional[Dict[str, str]] = None):
        super().__init__(field_id)
        if style is None:
            style = {}
        self._children = children
        self._style = style
    
    def markup(self, env: Environment, info: Dict[str, Any]) -> Markup:
        return Markup(env.get_template("fields/view/container.html").render(
                id=self.id,
                children=[child.markup(env, info) for child in self._children],
                style=style2str(self._style)
        ))
