from flask import Flask, request

from flask_simple_ui import UI
from flask_simple_ui.form import Form
from flask_simple_ui.input import Text, Submit, File
from flask_simple_ui.login import PasswordHandler
from flask_simple_ui.output import ButtonGroup, Table, Paragraph, Heading, Container
from flask_simple_ui.view import View

app = Flask(__name__)

ui = UI(app)

test_form = Form([
    Text("text", "Text", required=True),
    Text("text2", "Text2"),
    ButtonGroup("buttons", [Submit("submit", "Submit")])
])


@ui.input("/", test_form, template_args={"title": "sample", "heading": "Sample"})
def test(info, data):
    t_data = []
    for key in data:
        t_data.append([key, data[key]])
    return ui.view(View([Table("table", t_data, numbered=True, head=["key", "data"])]))


file_form = Form([
    File("file", "File", required=True),
    ButtonGroup("buttons", [Submit("submit", "Submit")])
])


@ui.input("/read", file_form,
          template_args={
              "title":   "sample - read file as hex",
              "heading": "Sample - Read file as hex"
          })
def test(info, data):
    return ui.view(
            View([
                Heading(
                        "title",
                        1,
                        "Hex dump",
                        style={
                            "text-align": "center"
                        }
                ),
                Paragraph(
                        "result",
                        data["file"].stream.read().hex(),
                        style={
                            "word-break": "break-all"
                        }
                )
            ])
    )


admin = UI(app, login_handler=PasswordHandler("123456"), root="/admin", csrf_secret="654321")

secret_form = Form([
    Text("secret", "Secret", required=True),
    ButtonGroup("buttons", [Submit("submit", "Submit")])
])


@admin.input("/", secret_form, template_args={"title": "secret - sample", "heading": "Secret"})
def secret(info, data):
    t_data = []
    for key in data:
        t_data.append([key, data[key]])
    return ui.form(secret_form, template_args={
        "errors": ["Some error", "Another error"], "warnings": ["A warning"], "success": "Secret received"
    })


if __name__ == '__main__':
    app.run("localhost", 7823, debug=True)
