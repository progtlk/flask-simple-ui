from typing import *

from jinja2 import Environment
from markupsafe import Markup
from wtforms import FormField, StringField, FileField
from wtforms.validators import DataRequired

from .form import UIFormField
from .output import Button


class InputField(UIFormField):
    def __init__(self, field_id: str, label: str):
        super().__init__(field_id)
        self._label = label
    
    @property
    def wtf(self) -> Optional[FormField]:
        raise NotImplementedError
    
    def markup(self, env: Environment, info: Dict[str, Any]) -> Markup:
        raise NotImplementedError


class Text(InputField):
    def __init__(self, field_id: str, label: str, required=False, value=None):
        super().__init__(field_id, label)
        self._required = required
        self._value = value
    
    @property
    def wtf(self) -> Optional[FormField]:
        validators = []
        if self._required:
            validators.append(DataRequired())
        return StringField(self._label, validators)
    
    def markup(self, env: Environment, info: Dict[str, Any]) -> Markup:
        return Markup(env.get_template("fields/input/text.html").render(id=self.id, label=self._label,
                                                                        required=self._required, value=self._value))


class Password(Text):
    def markup(self, env: Environment, info: Dict[str, any]) -> Markup:
        return Markup(env.get_template("fields/input/text.html").render(id=self.id, label=self._label,
                                                                        required=self._required, type="password",
                                                                        value=self._value))


class File(InputField):
    def __init__(self, field_id: str, label: str, required=False):
        super().__init__(field_id, label)
        self._required = required
    
    @property
    def wtf(self) -> Optional[FormField]:
        validators = []
        if self._required:
            validators.append(DataRequired())
        return FileField(self._label, validators)
    
    def markup(self, env: Environment, info: Dict[str, any]) -> Markup:
        return Markup(
                env.get_template("fields/input/file.html").render(
                        id=self.id,
                        label=self._label,
                        required=self._required
                )
        )


class Hidden(UIFormField):
    @property
    def wtf(self) -> Optional[FormField]:
        return StringField(self._id, validators=DataRequired())

    def __init__(self, field_id: str, value: str):
        super().__init__(field_id)
        self._value = value

    def markup(self, env: Environment, info: Dict[str, any]) -> Markup:
        return Markup(env.get_template("fields/input/hidden.html").render(id=self.id, value=self._value))


class Submit(Button):
    def __init__(self, field_id: str, label: str):
        super().__init__(field_id, label)
    
    def markup(self, env: Environment, info: Dict[str, Any]) -> Markup:
        return Markup(env.get_template("fields/input/submit.html").render(label=self._label, id=self.id))
