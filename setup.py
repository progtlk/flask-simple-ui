from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
        name="flask-simple-ui",
        version="0.1.1",
        packages=["flask_simple_ui"],
        package_dir={"": "src"},
        url="https://gitlab.com/cromefire_/flask-simple-ui",
        license="MPL-2.0",
        author="Cromefire_",
        author_email="tim.langhorst@outlook.de",
        description="Make simple UIs with bootstrap 4",
        long_description=long_description,
        long_description_content_type="text/markdown",
        classifiers=[
            "Programming Language :: Python :: 3",
            "Programming Language :: Python :: 3 :: Only",
            "License :: OSI Approved :: Mozilla Public License 2.0 (MPL 2.0)",
            "Operating System :: OS Independent",
            "Framework :: Flask"
        ],
        install_requires=[
            "jinja2",
            "markupsafe",
            "wtforms",
            "flask",
            "werkzeug"
        ],
        package_data={
            "": ["*.html"]
        },
        test_suite="nose.collector",
        tests_require=['nose']
)
