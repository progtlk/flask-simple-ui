# Flask Simple UI

To get started you need a simple flask app:
``` python
from flask import Flask

app = Flask(__name__)
```

Attaching a UI is very simple:
``` python
from flask_simple_ui import UI

ui = UI(app)
```

You can also attach it to another root or require a password:
``` python
admin = UI(app, login_handler=PasswordHandler("123456"), root="/admin")
```

An view can be defined like this:
``` python
test_form = Form([Text("text", "Text", required=True), Text("text2", "Text2"),
                  ButtonGroup("buttons", [Submit("submit", "Submit")])])


@ui.input("/", test_form, template_args={"title": "sample", "heading": "Sample"})
def test(info, data):
    t_data = []
    for key in data:
        t_data.append([key, data[key]])
    return ui.view(View([Table("table", t_data, numbered=True, head=["key", "data"])]))
```

You can also return a form again (for `@UI.input` routes this has to be the same
form again) and pass parameters to the template (like: `errors` (`str[]`), `warnings`
(`str[]`) or `success` (`str`))
``` python
ui.form(
    secret_form,
    template_args={
        "errors": [
            "Some error",
            "Another error"
        ],
        "warnings": [
            "A warnings"
        ],
        "success": "Secret received"
    }
)
```

You can also use this library for simple outputs:
``` python
@ui.route("/", template_args={"title": "sample", "heading": "Sample"})
def test(info):
    return ui.view(View([Table("table", [["key1", "value1"]], numbered=True, head=["key", "data"])]))
```

For more examples see `src/samples/sample.py`

# Planned
- CSRF
